package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.Document;

class DocumentTester {

	/*
	 * Things to consider testing:
	 * 
	 * Parsing embedded documents
	 * Parsing arrays
	 * 
	 * Object to primitive
	 * Object to embedded document
	 * Object to array
	 */
	
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	
	@Test
	public void testParse() {
		String json = "{ \"key\": \"value\" }";
		JsonObject results = Document.parse(json);
		assertTrue(results.getAsJsonPrimitive("key").getAsString().equals("value"));
	}
	@Test
	public void testToStr() {
		String json = "{ \"key\": \"value\" }";
		JsonObject results = Document.parse(json);
		assertTrue(results.getAsJsonPrimitive("key").getAsString().equals("value"));
		System.out.println(results.toString());
		assertTrue(true);
	}
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file

}
