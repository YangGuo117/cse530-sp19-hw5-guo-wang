package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import hw5.DB;

class DBTester {

	/*
	 * Things to consider testing:
	 * 
	 * Properly returns collections
	 * Properly creates new collection
	 * Properly creates new DB (done)
	 * Properly drops db
	 */
	
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	
	@Test
	void testCreateNewDB() {
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
	}
	@Test
	void testGetCollection() {
		DB hw5 = new DB("hw5");
		hw5.getCollection("tesst");
		assertTrue(new File("testfiles/hw5").exists());
		assertTrue(new File("testfiles/hw5/tesst.json").exists());
	}
//	@Test
	void testDeleteDB() {
		DB hw5 = new DB("hw5");
		hw5.getCollection("tesst");
		assertTrue(new File("testfiles/hw5").exists());
		hw5.dropDatabase();
		assertFalse(new File("testfiles/hw5").exists());
	}
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file

}
