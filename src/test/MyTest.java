package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class MyTest {

	
	private DB testDB= new DB("MyTest");
	private DBCollection collection= testDB.getCollection("collection");
	// I print the result of the query on the console. It is more easier for us 
	//to see whether the query performed right 
	// All the data be test is listed in the reserDataSet() function
	private void resetDataSet() {
		collection.drop();
		collection.insert(
				Document.parse("{ item: 'journal', qty: 25, size: { h: 14, w: 21, uom: 'cm' }, status: 'A', tags: ['blank', 'red'], dim_cm: [ 14, 21 ],instock: [ { warehouse: 'A', qty: 5 }, { warehouse: 'C', qty: 15 } ]}"),
		        Document.parse("{ item: 'notebook', qty: 50, size: { h: 8.5, w: 11, uom: 'in' }, status: 'A', tags: ['red', 'blank'], dim_cm: [ 14, 21 ], instock: [ { warehouse: 'C', qty: 5 } ]}"),
		        Document.parse("{ item: 'paper', qty: 100, size: { h: 8.5, w: 11, uom: 'in' }, status: 'D' , tags: ['red', 'blank', 'plain'], dim_cm: [ 14, 21 ], instock: [ { warehouse: 'A', qty: 60 }, { warehouse: 'B', qty: 15 } ] }"),
		        Document.parse("{ item: 'planner', qty: 75, size: { h: 22.85, w: 30, uom: 'cm' }, status: 'D' , tags: ['blank', 'red'], dim_cm: [ 22.85, 30 ], instock: [ { warehouse: 'A', qty: 40 }, { warehouse: 'B', qty: 5 } ]}"),
		        Document.parse("{ item: 'postcard', qty: 45, size: { h: 10, w: 15.25, uom: 'cm' }, status: 'A' , tags: ['blue'], dim_cm: [ 10, 15.25 ], instock: [ { warehouse: 'B', qty: 15 }, { warehouse: 'C', qty: 35 } ]}"));
	}
	
	@Test
	void testDB() {
		// test create new DB, create collection, and drop database
		DB tempDB = new DB("MyTest1");
		tempDB.getCollection("collection1");
		DBCollection cl2 = tempDB.getCollection("collection2");
		assertTrue(new File("testfiles/MyTest1").exists()); // test DB exist
		assertTrue(new File("testfiles/MyTest1/collection1.json").exists()); // test collection exist
		assertTrue(new File("testfiles/MyTest1/collection2.json").exists());
		cl2.drop();
		assertFalse(new File("testfiles/MyTest1/collection2.json").exists());
		tempDB.dropDatabase();
		assertFalse(new File("testfiles/MyTest1").exists()); // test drop
	}
	@Test
	void testDocument() {
		// test all kinds of Json type, primitive, object and array
		String json = "{ item: 'journal', qty: 25, size: { h: 14, w: 21, uom: 'cm' },"
				+ "status: 'A', tags: ['blank', 'red'], dim_cm: [ 14, 21 ],"
				+ "instock: [ { warehouse: 'A', qty: 5 }, { warehouse: 'C', qty: 15 } ]}";
		JsonObject results = Document.parse(json);
		assertTrue(results.getAsJsonPrimitive("item").getAsString().equals("journal"));
		assertTrue(results.get("size").isJsonObject());
		assertTrue(results.get("tags").isJsonArray());
		assertTrue(results.getAsJsonArray("instock").get(0).getAsJsonObject().getAsJsonPrimitive("warehouse").getAsString().equals("A"));
		// test to string here, the result can be seen on the console
		System.out.println("The result of: parse "+json + ":\n" +results.toString());
		assertTrue(true);
	}
	@Test
	void testInsertToCollection() {
		// test insert multiple
		DB tempDB = new DB("MyTest1");
		DBCollection tempCollection = tempDB.getCollection("collection1");
		tempCollection.insert(Document.parse("{ item: 'journal', qty: 25, size: { h: 14, w: 21, uom: 'cm' }, status: 'A', tags: ['blank', 'red'], dim_cm: [ 14, 21 ],instock: [ { warehouse: 'A', qty: 5 }, { warehouse: 'C', qty: 15 } ]}"),
		        Document.parse("{ item: 'notebook', qty: 50, size: { h: 8.5, w: 11, uom: 'in' }, status: 'A', tags: ['red', 'blank'], dim_cm: [ 14, 21 ], instock: [ { warehouse: 'C', qty: 5 } ]}"),
		        Document.parse("{ item: 'paper', qty: 100, size: { h: 8.5, w: 11, uom: 'in' }, status: 'D' , tags: ['red', 'blank', 'plain'], dim_cm: [ 14, 21 ], instock: [ { warehouse: 'A', qty: 60 }, { warehouse: 'B', qty: 15 } ] }"),
		        Document.parse("{ item: 'planner', qty: 75, size: { h: 22.85, w: 30, uom: 'cm' }, status: 'D' , tags: ['blank', 'red'], dim_cm: [ 22.85, 30 ], instock: [ { warehouse: 'A', qty: 40 }, { warehouse: 'B', qty: 5 } ]}"),
		        Document.parse("{ item: 'postcard', qty: 45, size: { h: 10, w: 15.25, uom: 'cm' }, status: 'A' , tags: ['blue'], dim_cm: [ 10, 15.25 ], instock: [ { warehouse: 'B', qty: 15 }, { warehouse: 'C', qty: 35 } ]}"));
		assertTrue(tempCollection.count() == 5);
		tempDB.dropDatabase();
	}
	@Test
	void testLoadFromDisk() {
		resetDataSet();
		// test load collection from disk
		DBCollection newCollection = testDB.getCollection("collection");
		assertTrue(newCollection.count() == 5);
		collection.drop();
	}
	@Test
	void testFindAll() {
		System.out.println("------------ findAll test -----------------");
		System.out.println();
		resetDataSet();
		// since find() will return a cursor, so this is also a test of cursor
		DBCursor cursor = collection.find();
		System.out.println("findAll()");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 5);
		collection.drop();
	}
	@Test
	void testQueryEmbeddedDoc() {
		System.out.println("------------ query on Enbedded Document test -----------------");
		System.out.println();
		resetDataSet();
		// test query basic embedded doc
		String query1 = "{ 'size.uom': 'in' }";
		JsonObject query = Document.parse(query1);
		DBCursor cursor = collection.find(query);
		System.out.println("find( "+   query1   +" )");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 2);
		
		// test query with operator
		String query2 = "{ 'size.h': { $lt: 15 } }";
		query = Document.parse(query2);
		cursor = collection.find(query);
		System.out.println("find( "+   query2   +" )");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 4);
		
		// test complex query
		String query3 = "{ 'size.h': { $lt: 15 }, 'size.uom': 'in', status: 'D' }";
		query = Document.parse(query3);
		cursor = collection.find(query);
		System.out.println("find( "+   query3   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 1);
		collection.drop();
	}
	@Test
	void testQueryArray() {
		System.out.println("------------ query an Array test -----------------");
		System.out.println();
		resetDataSet();
		// queries for all documents where the field tags value is an array with exactly two elements
		String query1 = "{ tags: ['red', 'blank'] }";
		JsonObject query = Document.parse(query1);
		DBCursor cursor = collection.find(query);
		System.out.println("find( "+   query1   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 1);
		
		// queries for all documents where tags is an array that contains the string "red" as one of its elements
		String query2 = "{ tags: 'red' }";
		query = Document.parse(query2);
		cursor = collection.find(query);
		System.out.println("find( "+   query2   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 4);
		
		// queries for documents where the dim_cm array contains at least one element that is both greater than ($gt) 22 and less than ($lt) 30
		String query3 = "{ dim_cm: { $gt: 22, $lt: 30 } }";
		query = Document.parse(query3);
		cursor = collection.find(query);
		System.out.println("find( "+   query3   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 1);
		
		//queries for all documents where the second element in the array dim_cm is greater than 25
		String query4 = "{ 'dim_cm.1': { $gt: 25 }}";
		query = Document.parse(query4);
		cursor = collection.find(query);
		System.out.println("find( "+   query4   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 1);
		collection.drop();
	}
	@Test
	void testArrayOfEmbeddedDoc() {
		System.out.println("------------ query an Array of Embeded Document test -----------------");
		System.out.println();
		resetDataSet();
		// selects all documents where an element in the instock array matches the specified document
		String query1 = "{ 'instock': { warehouse: 'A', qty: 5 } }";
		JsonObject query = Document.parse(query1);
		DBCursor cursor = collection.find(query);
		System.out.println("find( "+   query1   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 1);
		
		//selects all documents where the instock array has at least one embedded document that contains the field qty whose value is less than or equal to 20
		String query2 = "{ 'instock.qty': { $gte: 20 } }";
		query = Document.parse(query2);
		cursor = collection.find(query);
		System.out.println("find( "+   query2   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 3);
		collection.drop();
	}
	@Test
	void testProjection() {
		System.out.println("------------ projection test -----------------");
		System.out.println();
		resetDataSet();
		// this query will find two result
		String query1 = "{ 'size.uom': 'in' }";
		JsonObject query = Document.parse(query1);
		
		// project 1 with sample case and Embedded doc case, we can see result on console
		String project1 = "{ item: 1, status: 1, 'size.uom': 1 }";
		JsonObject project = Document.parse(project1);
		DBCursor cursor = collection.find(query,project);
		System.out.println("find( "+   query1   +", " +project1 +" ) ");
		while (cursor.hasNext()) {
			JsonObject next = cursor.next();
			System.out.println(Document.toJsonString(next));
			assertTrue(!next.has("tags")); // do not have tags
			assertTrue(next.getAsJsonObject("size").has("uom")); // size have uom
			assertTrue(!next.getAsJsonObject("size").has("h")); // size do not have h
		}
		System.out.println();
		
		// project 1 with sample case and Array case
		String project2 = "{item: 1, status: 1,'instock.qty': 1 }";
		project = Document.parse(project2);
		cursor = collection.find(query,project);
		System.out.println("find( "+   query1   +", " +project2 +" ) ");
		while (cursor.hasNext()) {
			JsonObject next = cursor.next();
			System.out.println(Document.toJsonString(next));
			assertTrue(!next.has("tags")); // do not have tags
			assertTrue(next.getAsJsonArray("instock").get(0).getAsJsonObject().has("qty")); //instock have qty
			assertTrue(!next.getAsJsonArray("instock").get(0).getAsJsonObject().has("warehouse")); //instock do not have warehouse
		}
		System.out.println();
		
		//project 0 with sample case , Array case, and Embedded doc case
		String project3 = "{instock.qty: 0,item: 0,size.h:0}";
		project = Document.parse(project3);
		cursor = collection.find(query,project);
		System.out.println("find( "+   query1   +", " +project3 +" ) ");
		while (cursor.hasNext()) {
			JsonObject next = cursor.next();
			System.out.println(Document.toJsonString(next));
			assertTrue(next.has("tags")); // has tags
			assertTrue(next.getAsJsonObject("size").has("uom")); // size have uom
			assertTrue(!next.getAsJsonObject("size").has("h")); // size do not have h
			assertTrue(!next.getAsJsonArray("instock").get(0).getAsJsonObject().has("qty")); //instock do not have qty
			assertTrue(next.getAsJsonArray("instock").get(0).getAsJsonObject().has("warehouse")); //instock have warehouse
		}
		System.out.println();
		assertTrue(true);
		collection.drop();
	}
	
	@Test
	void testRemove() {
		System.out.println("------------ remove test -----------------");
		System.out.println();
		resetDataSet();
		// delete many
		String query1 = "{ 'size.uom': 'in' }";
		JsonObject query = Document.parse(query1);
		collection.remove(query, true);
		collection = testDB.getCollection("collection"); // reload collection
		DBCursor cursor = collection.find();
		System.out.println("DeleteMany( "+   query1   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		assertTrue(cursor.count() == 3);
		System.out.println();
		
		resetDataSet();
		// delete one
		collection.remove(query, false);
		collection = testDB.getCollection("collection"); // reload collection
		cursor = collection.find();
		System.out.println("DeleteOne( "+   query1   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 4);
		collection.drop();
	}
	
	@Test
	void testUpdate() {
		System.out.println("------------ update test -----------------");
		System.out.println();
		resetDataSet();
		// we do not have operator like set, so update is actually replace
		// We can see the console to find whether our result is right
		// update many
		String query1 = "{ 'size.uom': 'in' }";
		JsonObject query = Document.parse(query1);
		collection.update(query,query, true);
		collection = testDB.getCollection("collection"); // reload collection
		DBCursor cursor = collection.find();
		System.out.println("ReplaceMany( "+   query1+ ", " + query1 +" ) ");
		int count = 0;
		while (cursor.hasNext()) {
			JsonObject next = cursor.next();
			System.out.println(Document.toJsonString(next));
			if(next.size() == 1) { // the object I update is size 1
				count += 1;
			}
		}
		System.out.println();
		assertTrue(count == 2); // 2 element are updated
		
		resetDataSet();
		//replace one
		collection.update(query,query, false);
		collection = testDB.getCollection("collection"); // reload collection
		cursor = collection.find();
		System.out.println("ReplaceOne( "+   query1+ ", " + query1 +" ) ");
		count = 0;
		while (cursor.hasNext()) {
			JsonObject next = cursor.next();
			System.out.println(Document.toJsonString(next));
			if(next.size() == 1) {
				count += 1;
			}
		}
		System.out.println();
		assertTrue(count == 1); // only one is updated
		collection.drop();
	}
	@Test
	void testInOperator() {
		System.out.println("------------ $in $nin test -----------------");
		System.out.println();
		resetDataSet();
		// query element in Array in Array? (element wise)
		String query1 = "{ 'instock.qty': { $in: [15,25] } }";
		JsonObject query = Document.parse(query1);
		DBCursor cursor = collection.find(query);
		System.out.println("find( "+   query1   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 3);
		
		// query element in Object in Array?
		String query2 = "{ 'size.w': { $in: [11,30] } }";
		query = Document.parse(query2);
		cursor = collection.find(query);
		System.out.println("find( "+   query2   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		assertTrue(cursor.count() == 3);
		System.out.println();
		
		// query array not in array
		String query3 = "{ 'instock.qty': { $nin: [15,25] } }";
		query = Document.parse(query3);
		cursor = collection.find(query);
		System.out.println("find( "+   query3   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 2);
		
		// query element in Object not in Array?
		String query4 = "{ 'size.w': { $nin: [11,30] } }";
		query = Document.parse(query4);
		cursor = collection.find(query);
		System.out.println("find( "+   query4   +" ) ");
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		System.out.println();
		assertTrue(cursor.count() == 2);
		collection.drop();
		
	}
	
}
