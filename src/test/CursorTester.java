package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class CursorTester {

	/*
	 *Queries:
	 * 	Find all (done?)
	 * 	Find with relational select
	 * 	Find with projection
	 * 	Conditional operators
	 * 	Embedded Documents and arrays
	 */
	
	
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	// Note all the test is in MyTest file
	
	
	@Test
	public void testFindAll() {
		DB db = new DB("data");
		DBCollection test = db.getCollection("test");
		DBCursor results = test.find();
		assertTrue(results.count() == 3);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); //verify contents?
		assertTrue(results.hasNext());
		JsonObject d2 = results.next();//verify contents?
		assertTrue(results.hasNext());
		JsonObject d3 = results.next();//verify contents?
		assertTrue(!results.hasNext());
	}
	
// Note all the test is in MyTest file
// Note all the test is in MyTest file
// Note all the test is in MyTest file
// Note all the test is in MyTest file
// Note all the test is in MyTest file

//	@Test
	void testQuery() {
		DB hw5 = new DB("hw5");
		DBCollection collection = hw5.getCollection("tesst");
		collection.insert(Document.parse("{ item: 'journal', qty: 25, size: { h: 14, w: 21, uom: 'cm' }, status: 'A', tags: ['blank', 'red'], dim_cm: [ 14, 21 ]}"),
		        Document.parse("{ item: 'notebook', qty: 50, size: { h: 8.5, w: 11, uom: 'in' }, status: 'A', tags: ['red', 'blank'], dim_cm: [ 14, 21 ]}"),
		        Document.parse("{ item: 'paper', qty: 100, size: { h: 8.5, w: 11, uom: 'in' }, status: 'D' , tags: ['red', 'blank', 'plain'], dim_cm: [ 14, 21 ]}"),
		        Document.parse("{ item: 'planner', qty: 75, size: { h: 22.85, w: 30, uom: 'cm' }, status: 'D' , tags: ['blank', 'red'], dim_cm: [ 22.85, 30 ]}"),
		        Document.parse("{ item: 'postcard', qty: 45, size: { h: 10, w: 15.25, uom: 'cm' }, status: 'A' , tags: ['blue'], dim_cm: [ 10, 15.25 ]}"));
		String query1 = "{ 'size.uom': 'in' }";
		String query2 = "{ 'size.h': { $lt: 15 } }";
		String query3 = "{ 'size.h': { $lt: 15 }, 'size.uom': 'in', status: 'D' }";
		
		JsonObject query = Document.parse(query3);
		DBCursor cursor = collection.find(query);
		
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		collection.drop();
		assertTrue(true);
	}
//	@Test
	void testArray() {
//		JsonObject test = Document.parse("{ item: 'journal', qty: 25, size: { h: 14, w: 21, uom: 'cm' }, status: 'A', tags: ['blank', 'red'], dim_cm: [ 14, 21 ]}");
//		JsonElement s = test.get("size.h");
		DB hw5 = new DB("hw5");
		DBCollection collection = hw5.getCollection("tesst");
		collection.insert(Document.parse("{ item: 'journal', qty: 25, size: { h: 14, w: 21, uom: 'cm' }, status: 'A', tags: ['blank', 'red'], dim_cm: [ 14, 21 ],instock: [ { warehouse: 'A', qty: 5 }, { warehouse: 'C', qty: 15 } ]}"),
		        Document.parse("{ item: 'notebook', qty: 50, size: { h: 8.5, w: 11, uom: 'in' }, status: 'A', tags: ['red', 'blank'], dim_cm: [ 14, 21 ], instock: [ { warehouse: 'C', qty: 5 } ]}"),
		        Document.parse("{ item: 'paper', qty: 100, size: { h: 8.5, w: 11, uom: 'in' }, status: 'D' , tags: ['red', 'blank', 'plain'], dim_cm: [ 14, 21 ], instock: [ { warehouse: 'A', qty: 60 }, { warehouse: 'B', qty: 15 } ] }"),
		        Document.parse("{ item: 'planner', qty: 75, size: { h: 22.85, w: 30, uom: 'cm' }, status: 'D' , tags: ['blank', 'red'], dim_cm: [ 22.85, 30 ], instock: [ { warehouse: 'A', qty: 40 }, { warehouse: 'B', qty: 5 } ]}"),
		        Document.parse("{ item: 'postcard', qty: 45, size: { h: 10, w: 15.25, uom: 'cm' }, status: 'A' , tags: ['blue'], dim_cm: [ 10, 15.25 ], instock: [ { warehouse: 'B', qty: 15 }, { warehouse: 'C', qty: 35 } ]}"));
		String query1 = "{ tags: ['red', 'blank'] }";
		String query2 = "{ tags: 'red' }";
		String query3 = "{ dim_cm: { $gt: 15, $lt: 20 } }";
		String query4 = "{ 'dim_cm.1': { $gt: 25 }}";
		String query5 = "{ 'instock': { warehouse: 'A', qty: 5 } }";
		String query6 = "{ 'instock.qty': { $gte: 20 } }";
		// projectiong
		String project1 = "{ item: 1, status: 1 }";
		String project2 = "{ item: 1, status: 1, 'size.uom': 1 }";
		String project3 = "{item: 1, status: 1,'instock.qty': 1 }";
		String project4 = "{instock.qty: 0,item: 0,size.h:0}";
		
		JsonObject query = Document.parse(query2);
		JsonObject project = Document.parse(project1);
		DBCursor cursor = collection.find(query,project);
		
		while (cursor.hasNext()) {System.out.println(Document.toJsonString(cursor.next()));}
		collection.drop();
		collection.drop();
		assertTrue(true);
	}

}
