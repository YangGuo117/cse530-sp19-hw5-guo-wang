package hw5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.google.gson.*;

public class Document {
	
	/**
	 * Parses the given json string and returns a JsonObject
	 * This method should be used to convert text data from
	 * a file into an object that can be manipulated.
	 */
	public static JsonObject parse(String json) {
		JsonParser parser=new JsonParser();
		JsonElement jsElem = parser.parse(json);
		if (jsElem.isJsonNull()) return null;
		JsonObject jsObj=(JsonObject) jsElem;
		return jsObj;
	}
	public static boolean isInteger(String str) {
	    Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");    
	    return pattern.matcher(str).matches();    
	}
	/**
	 * Takes the given object and converts it into a
	 * properly formatted json string. This method should
	 * be used to convert JsonObjects to strings
	 * when writing data to disk.
	 */
	public static String toJsonString(JsonObject json) { // {kay1:val1,key2:val2}
		String res = "{";
		Set<String> keySet = json.keySet();
		for (String key : keySet) {
			JsonElement jsElem = json.get(key);
			if (jsElem.isJsonPrimitive()) {
				res += "\"" + key + "\"" + ":"+ getPrimitive((JsonPrimitive)jsElem) +",";
			}
			if (jsElem.isJsonArray()) {
				res += "\"" +key + "\"" + ":"+ getArryay((JsonArray)jsElem) +",";
			}
			if (jsElem.isJsonObject()) {
				res += "\"" +key + "\"" + ":" + toJsonString((JsonObject)jsElem) +",";
			}
		}
		res = res.substring(0, res.length()-1);
		res += "}";
		return res;
	}
	
	private static String getPrimitive(JsonPrimitive jsPrim) { // value
		if (jsPrim.isNumber()) return jsPrim.getAsString();
		String res = "\"";
		res += jsPrim.getAsString();
		return res + "\"" ;
	}
	
	private static String getArryay(JsonArray jsArray) { // [val1,val2]
		String res = "[";
		for (JsonElement jsElem : jsArray) {
			if (jsElem.isJsonPrimitive()) {
				res += getPrimitive((JsonPrimitive)jsElem) +",";
			}
			if (jsElem.isJsonArray()) {
				res +=  getArryay((JsonArray)jsElem) +",";
			}
			if (jsElem.isJsonObject()) {
				res += toJsonString((JsonObject)jsElem) +",";
			}
		}
		res = res.substring(0, res.length()-1);
		return res+"]";
	}
}
