package hw5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.google.gson.JsonObject;

public class DBCollection {

	/**
	 * Constructs a collection for the given database
	 * with the given name. If that collection doesn't exist
	 * it will be created.
	 */
	private DB db;
	private String dir;
	private String name;
	private ArrayList<JsonObject> docObjs;
	public DBCollection(DB database, String name) {
		docObjs = new ArrayList<JsonObject>();
		this.name = name;
		db = database;
		dir = db.getDir();
		if (dir == null) return;
		File file = new File(dir);
		String[] docStrs = null;
		int size =0;
		try {
			FileInputStream in = new FileInputStream(file);
			size=in.available();
	        byte[] buffer=new byte[size];
	        in.read(buffer);
	        in.close();
	        String str=new String(buffer,"GB2312");
	        docStrs = str.split("\t");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (size == 0) return;
		for (String docStr : docStrs) {
			JsonObject docObj = Document.parse(docStr);
			if (docObj == null) continue;
			docObjs.add(docObj);
		}
	}
	public ArrayList<JsonObject> getDocs() {
		return docObjs;
	}
	
	/**
	 * Returns a cursor for all of the documents in
	 * this collection.
	 */
	public DBCursor find() {
		return new DBCursor(this);
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @return
	 */
	public DBCursor find(JsonObject query) {
		JsonObject fields = Document.parse("{ 'mmulType': 'many' }");
		return new DBCursor(this,query,fields);
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @param projection relational project
	 * @return
	 */
	public DBCursor find(JsonObject query, JsonObject projection) {
		return new DBCursor(this,query,projection);
	}
	
	/**
	 * Inserts documents into the collection
	 * Must create and set a proper id before insertion
	 * When this method is completed, the documents
	 * should be permanently stored on disk.
	 * @param documents
	 */
	public void insert(JsonObject... documents) {
		for (JsonObject docObj : documents) {
        	this.docObjs.add(docObj);
		}
		writeToDisk();
	}
	private void writeToDisk() {
		File file =new File(dir);
		// flush the old one
		try {
            FileWriter fileWriter =new FileWriter(file);
            fileWriter.write("");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
		// write the whole collection to disk
		try {
            FileWriter fw = new FileWriter(dir, true);
            BufferedWriter bw = new BufferedWriter(fw);
            for (JsonObject docObj : this.docObjs) {
    			String docStr = Document.toJsonString(docObj);
    			bw.write(docStr+ "\n\t\n");
    		}
            bw.close();
            fw.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	/**
	 * Locates one or more documents and replaces them
	 * with the update document.
	 * @param query relational select for documents to be updated
	 * @param update the document to be used for the update
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void update(JsonObject query, JsonObject update, boolean multi) {
		JsonObject fields;
		if (multi) fields= Document.parse("{ 'mmulType': 'many' }");
		else fields= Document.parse("{ 'mmulType': 'one' }");
		DBCursor cursor = new DBCursor(this,query,fields);
		HashSet<Integer> modifyIndexs = cursor.getIndexs();
//		ArrayList<JsonObject> newObjs = new ArrayList<JsonObject>();
//		while (cursor.hasNext()) {
//			newObjs.add(cursor.next());
//		}
//		docObjs = newObjs;
		for (int i = 0; i < this.docObjs.size(); i++) {
			// if i is modified, set that to update
			if (modifyIndexs.contains(i) && cursor.hasNext()) docObjs.set(i, update);
		}
		writeToDisk();
	}
	
	/**
	 * Removes one or more documents that match the given
	 * query parameters
	 * @param query relational select for documents to be removed
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void remove(JsonObject query, boolean multi) {
		JsonObject fields;
		if (multi) fields= Document.parse("{ 'mmulType': 'many' }");
		else fields= Document.parse("{ 'mmulType': 'one' }");
		DBCursor cursor = new DBCursor(this,query,fields);
		HashSet<Integer> removeIndexs = cursor.getIndexs();
		ArrayList<JsonObject> newObjs = new ArrayList<JsonObject>();
		for (int i = 0; i < this.docObjs.size(); i++) {
			// if i is not removed, add to tempArr
			if (!removeIndexs.contains(i)) newObjs.add(docObjs.get(i));
		}
		docObjs = newObjs;
		writeToDisk();
	}
	
	/**
	 * Returns the number of documents in this collection
	 */
	public long count() {
		return this.docObjs.size();
	}
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns the ith document in the collection.
	 * Documents are separated by a line that contains only a single tab (\t)
	 * Use the parse function from the document class to create the document object
	 */
	public JsonObject getDocument(int i) {
		return this.docObjs.get(i);
	}
	
	/**
	 * Drops this collection, removing all of the documents it contains from the DB
	 */
	public void drop() {
		File file = new File(dir);
		docObjs.clear();
		if (file.exists()) file.delete();
	}
	
}
