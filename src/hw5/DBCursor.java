package hw5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class DBCursor implements Iterator<JsonObject>{
	private ArrayList<JsonObject> cursorObjs;
	private HashSet<Integer> indexs;
	private int index = 0;
	
	public DBCursor(DBCollection collection) { // find all
		cursorObjs = new ArrayList<JsonObject>();
		indexs = new HashSet<Integer>();
		ArrayList<JsonObject> docObjs = collection.getDocs();
		cursorObjs = docObjs;
	}
	private JsonObject projection(JsonObject doc, JsonObject fields) {
		JsonObject res = new JsonObject();
		Boolean isOne = false; 
		Boolean isZero =false;
		for (Entry<String, JsonElement> entry: fields.entrySet()) {
			String keyStr = entry.getKey();
			String[] keys = keyStr.split("\\.");
			JsonElement operator = entry.getValue();
			isOne = operator.getAsInt() == 1; // I assume operators are all 0 or all 1
			isZero = operator.getAsInt() == 0;
			// find target
			String[] subKeys = Arrays.copyOfRange(keys, 0, keys.length-1);
			JsonElement parent = findResult(subKeys,doc);
			JsonElement target = findResult(keys,doc);// child, this must be run immediately before reConstruct, to get the right type
			if (isOne) { //create a new Object
				if (target == null || parent == null || target.isJsonNull()) { // query did not match anything
					// insert {}
					JsonObject temp = Document.parse("{}");
					res.add(keys[0], temp);
				} else {
					// insert reconstruct obj
					JsonElement reCons = reConstruct(keys,target,doc);
					res.add(keys[0], reCons);
				}
			}
			if (target == null || parent == null || target.isJsonNull()) continue;
			if (isZero) { // delete the child from parent
				if (parent.isJsonArray()) {
					if (target.isJsonArray()) {
						for (JsonElement tempTarget: target.getAsJsonArray()) {
							if (parent.getAsJsonArray().get(0).isJsonPrimitive()) parent.getAsJsonArray().remove(tempTarget);
							else if (parent.getAsJsonArray().get(0).isJsonObject()) {
								for (JsonElement parElem:parent.getAsJsonArray()) {
									parElem.getAsJsonObject().remove(keys[keys.length-1]);
								}
							}
						}
					}
					if (target.isJsonObject()) parent.getAsJsonArray().remove(target);
				}
				if (parent.isJsonObject()) {
					parent.getAsJsonObject().remove(keys[keys.length-1]);
				}
			}
		}
		if (isZero) return doc;
		return res; // isOne
	}
	private JsonElement reConstruct(String[] keys,JsonElement target,JsonObject doc) {
		JsonElement res = target;
		ArrayList<String> curType = (ArrayList<String>) type.clone();
		for (int i = keys.length-1; i >= 1 ; i--) {
			String keyWord = keys[i];
			String[] subKeys = Arrays.copyOfRange(keys,0,i);
			if (Document.isInteger(keyWord)) {
				JsonElement tempp= this.findResult(subKeys, doc);
				if (tempp.isJsonObject()) {
					keyWord = (String) tempp.getAsJsonObject().keySet().toArray()[0];
				}
			}
			if (curType.get(i).equals("Arr")) {
				JsonArray temp = new JsonArray();
				if (res.isJsonArray()) {
					for (JsonElement arrElem:res.getAsJsonArray()) {
						JsonObject arrTemp = new JsonObject();
						arrTemp.add(keyWord, arrElem);
						temp.add(arrTemp);
						res = temp;
					}
				}
				if (res.isJsonObject() || res.isJsonPrimitive()) { temp.add(res); res = temp;}
			}
			if (curType.get(i).equals("Obj")) {
				JsonObject temp = new JsonObject();
				temp.add(keyWord, res);
				res = temp;
			}
		}
		return res;
	}
	private ArrayList<String> type;
	private JsonElement findResult(String[] keys, JsonElement doc) {
		type = new  ArrayList<String>();
		JsonElement target = doc;
		for (int i = 0; i < keys.length;i++) {
			if (target == null || target.isJsonNull()) return null;
			if (target.isJsonArray()) {
				if (Document.isInteger(keys[i])) { // if key is number for an array, I treat it as index
					JsonArray tmp = target.getAsJsonArray();
					int tIndex = Integer.parseInt(keys[i]);
					if (tIndex>=0 && tIndex<tmp.size()) {target = tmp.get(tIndex);type.add("Arr");}
					else target = null;
				} else { // if is not index
					JsonArray res = new JsonArray();
					for (JsonElement tempElem : target.getAsJsonArray()) {
						if (tempElem.isJsonObject()) { // for each element is JsObject
							JsonObject tmp = tempElem.getAsJsonObject();
							if (tmp.has(keys[i])) res.add(tmp.get(keys[i]));
						}
						if (tempElem.isJsonArray()) { // if each element is JsArray, I think the key should be index
							if (Document.isInteger(keys[i])) {
								JsonArray tmp = target.getAsJsonArray();
								int tIndex = Integer.parseInt(keys[i]);
								if (tIndex>=0 && tIndex<tmp.size()) res.add(tmp.get(tIndex));
							}
						}
					}
					if (!res.isJsonNull()) { target = res; type.add("Arr"); }
					else target = null;
				}
			}
			else if (target.isJsonObject()) {
				JsonObject tmp = target.getAsJsonObject();
				if (tmp.has(keys[i])) { target = tmp.get(keys[i]); type.add("Obj"); }
				else target = null;
			}
		}
		return target;
	}
	
	public DBCursor(DBCollection collection, JsonObject query, JsonObject fields) {
		cursorObjs = new ArrayList<JsonObject>();
		indexs = new HashSet<Integer>();
		ArrayList<JsonObject> docObjs = collection.getDocs();
		Boolean isProject=false;
		Boolean isMultiple = true;
		if (fields.has("mmulType")) {
			String mulType = fields.get("mmulType").getAsString();
			if (mulType.equals("one")) isMultiple = false;
		} else {
			isProject = true;
		}
		
		Set<Entry<String, JsonElement>> queryMap = query.entrySet();
		for (int iii = 0; iii < docObjs.size(); iii++) {
			JsonObject docObj = docObjs.get(iii);
			Boolean flag = true; // whether this docObj meet the condition of the query
			
			for (Entry<String, JsonElement> entry: queryMap) {
				if (flag == false) continue; // if it already not meet the condition,we just skip
				String keyStr = entry.getKey();
				String[] keys = keyStr.split("\\.");
				// find the result the query is looking for in the current jsObj
//				JsonElement target = docObj;
//				JsonElement result = null;
//				for (int i = 0; i < keys.length;i++) {
//					if (i != keys.length -1) { // not the last one
//						if (target.isJsonArray()) {
//							if (Document.isInteger(keys[i])) {
//								JsonArray tmp = target.getAsJsonArray();
//								int tIndex = Integer.parseInt("keys[i]");
//								if (tIndex>=0 && tIndex<tmp.size()) target = tmp.get(tIndex);
//							}
//						}
//						if (target.isJsonObject()) {
//							JsonObject tmp = target.getAsJsonObject();
//							if (tmp.has(keys[i])) target = tmp.get(keys[i]);
//						}
//					} else {// is last one
//						if (target.isJsonArray()) {
//							if (Document.isInteger(keys[i])) {
//								JsonArray tmp = target.getAsJsonArray();
//								int tIndex = Integer.parseInt(keys[i]);
//								if (tIndex>=0 && tIndex<tmp.size()) result = tmp.get(tIndex);
//							} else {
//								JsonArray res = new JsonArray();
//								for (JsonElement tempElem : target.getAsJsonArray()) {
//									if (tempElem.isJsonObject()) {
//										JsonObject tmp = tempElem.getAsJsonObject();
//										if (tmp.has(keys[i])) res.add(tmp.get(keys[i]));
//									}
//									if (!res.isJsonNull())result = res;
//								}
//							}
//						}
//						if (target.isJsonObject()) {
//							JsonObject tmp = target.getAsJsonObject();
//							if (tmp.has(keys[i])) result = tmp.get(keys[i]);
//						}
//					}
//				}
				
				// the above is my original design. I do not delete it, just in case.
				// find the result the query is looking for in the current jsObj
				JsonElement result = this.findResult(keys, docObj);
				if (result == null || result.isJsonNull()) flag = false;
				if (flag == false) continue;

				
				// find what query
				JsonElement queryElem = entry.getValue();
				if (queryElem.isJsonPrimitive()) { // this is in the case like  find( { "size.uom": "in" } )	
					if (result.isJsonPrimitive()) {
						flag = result.getAsString().equals(queryElem.getAsString());
					}
					if (result.isJsonArray()) {
						Boolean tempFlag = false; // if one match then match
						for (JsonElement arrayElem: (JsonArray) result) {
							if (arrayElem.isJsonPrimitive()) {
								JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
								if (arrayPrim.isNumber()) {// number
									tempFlag = tempFlag || arrayPrim.getAsFloat() == queryElem.getAsFloat();
								} else { // string, I don not think other type is comparable
									tempFlag = tempFlag || arrayPrim.getAsString().equals(queryElem.getAsString());
								}
							}
						}
						flag = tempFlag;
					}
					
					
				} else if (queryElem.isJsonObject()) { // query body is a jsObj i.e { "size.h": { $lt: 15 }}
					JsonObject queryObj = (JsonObject) queryElem;
					Boolean isCompared = false;
					Boolean arrFlag = true;
					Boolean isArr = false;
					if (queryObj.has("$eq")) {
						isCompared = true;
						if (queryObj.get("$eq").isJsonPrimitive()) { // can only handle primitive type after Operators
							JsonPrimitive queryPrim = queryObj.getAsJsonPrimitive("$eq");
							if (result.isJsonPrimitive()) { // handle primitive result
								JsonPrimitive resultPrim = result.getAsJsonPrimitive();
								if (queryPrim.isNumber()) {// number
									flag = resultPrim.getAsFloat() == queryPrim.getAsFloat();
								} else { // string, I don not think other type is comparable
									flag = resultPrim.getAsString().equals(queryPrim.getAsString());
								}
							} else if(result.isJsonArray()) { // handle Array result
								isArr = true;
								Boolean tempFlag = false; // if one match then match
								for (JsonElement arrayElem: (JsonArray) result) {
									if (arrayElem.isJsonPrimitive()) {
										JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
										if (arrayPrim.isNumber()) {// number
											tempFlag = tempFlag || arrayPrim.getAsFloat() == queryPrim.getAsFloat();
										} else { // string, I don not think other type is comparable
											tempFlag = tempFlag || arrayPrim.getAsString().equals(queryPrim.getAsString());
										}
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						} else {flag = false;}
					}
					if (queryObj.has("$gt")) {
						isCompared = true;
						if (queryObj.get("$gt").isJsonPrimitive()) { // can only handle primitive type after Operators
							JsonPrimitive queryPrim = queryObj.getAsJsonPrimitive("$gt");
							if (result.isJsonPrimitive()) { // handle primitive result
								JsonPrimitive resultPrim = result.getAsJsonPrimitive();
								if (queryPrim.isNumber()) {// number
									flag = resultPrim.getAsFloat() > queryPrim.getAsFloat();
								} else { // string, I don not think other type is comparable
									flag = resultPrim.getAsString().compareTo(queryPrim.getAsString()) > 0;
								}
							} else if(result.isJsonArray()) { // handle Array result
								isArr = true;
								Boolean tempFlag = false; // if one match then match
								for (JsonElement arrayElem: (JsonArray) result) {
									if (arrayElem.isJsonPrimitive()) {
										JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
										if (arrayPrim.isNumber()) {// number
											tempFlag = tempFlag || arrayPrim.getAsFloat() > queryPrim.getAsFloat();
										} else { // string, I don not think other type is comparable
											tempFlag = tempFlag || arrayPrim.getAsString().compareTo(queryPrim.getAsString()) > 0;
										}
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						} else {flag = false;}
					}
					if (queryObj.has("$gte")) {
						isCompared = true;
						if (queryObj.get("$gte").isJsonPrimitive()) { // can only handle primitive type after Operators
							JsonPrimitive queryPrim = queryObj.getAsJsonPrimitive("$gte");
							if (result.isJsonPrimitive()) { // handle primitive result
								isArr = true;
								JsonPrimitive resultPrim = result.getAsJsonPrimitive();
								if (queryPrim.isNumber()) {// number
									flag = resultPrim.getAsFloat() >= queryPrim.getAsFloat();
								} else { // string, I don not think other type is comparable
									flag = resultPrim.getAsString().compareTo(queryPrim.getAsString()) >= 0;
								}
							} else if(result.isJsonArray()) { // handle Array result
								isArr = true;
								Boolean tempFlag = false; // if one match then match
								for (JsonElement arrayElem: (JsonArray) result) {
									if (arrayElem.isJsonPrimitive()) {
										JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
										if (arrayPrim.isNumber()) {// number
											tempFlag = tempFlag || arrayPrim.getAsFloat() >= queryPrim.getAsFloat();
										} else { // string, I don not think other type is comparable
											tempFlag = tempFlag || arrayPrim.getAsString().compareTo(queryPrim.getAsString()) >= 0;
										}
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						} else {flag = false;}
					}
					if (queryObj.has("$in")) {
						isCompared = true;
						if (!queryObj.get("$in").isJsonArray()) flag = false;
						else {
							Boolean tempFlag = false;
							if (result.isJsonObject() || result.isJsonPrimitive()) {
								for (JsonElement queryJse : queryObj.getAsJsonArray("$in")) {
									tempFlag = tempFlag || queryJse.equals(result);
								}
								flag = tempFlag;
							}
							else if (result.isJsonArray()) {
								isArr = true;
								for (JsonElement resultElem : result.getAsJsonArray()) {
									for (JsonElement queryJse : queryObj.getAsJsonArray("$in")) {
										tempFlag = tempFlag || queryJse.equals(resultElem);
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						}
					}
					if (queryObj.has("$lt")) {
						isCompared = true;
						if (queryObj.get("$lt").isJsonPrimitive()) { // can only handle primitive type after Operators
							JsonPrimitive queryPrim = queryObj.getAsJsonPrimitive("$lt");
							if (result.isJsonPrimitive()) { // handle primitive result
								JsonPrimitive resultPrim = result.getAsJsonPrimitive();
								if (queryPrim.isNumber()) {// number
									flag = resultPrim.getAsFloat() < queryPrim.getAsFloat();
								} else { // string, I don not think other type is comparable
									flag = resultPrim.getAsString().compareTo(queryPrim.getAsString()) < 0;
								}
							} else if(result.isJsonArray()) { // handle Array result
								isArr = true;
								Boolean tempFlag = false; // if one match then match
								for (JsonElement arrayElem: (JsonArray) result) {
									if (arrayElem.isJsonPrimitive()) {
										JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
										if (arrayPrim.isNumber()) {// number
											tempFlag = tempFlag || arrayPrim.getAsFloat() < queryPrim.getAsFloat();
										} else { // string, I don not think other type is comparable
											tempFlag = tempFlag || arrayPrim.getAsString().compareTo(queryPrim.getAsString()) < 0;
										}
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						} else {flag = false;}
					}
					if (queryObj.has("$lte")) {
						isCompared = true;
						if (queryObj.get("$lte").isJsonPrimitive()) { // can only handle primitive type after Operators
							JsonPrimitive queryPrim = queryObj.getAsJsonPrimitive("$lte");
							if (result.isJsonPrimitive()) { // handle primitive result
								JsonPrimitive resultPrim = result.getAsJsonPrimitive();
								if (queryPrim.isNumber()) {// number
									flag = resultPrim.getAsFloat() <= queryPrim.getAsFloat();
								} else { // string, I don not think other type is comparable
									flag = resultPrim.getAsString().compareTo(queryPrim.getAsString()) <= 0;
								}
							} else if(result.isJsonArray()) { // handle Array result
								isArr = true;
								Boolean tempFlag = false; // if one match then match
								for (JsonElement arrayElem: (JsonArray) result) {
									if (arrayElem.isJsonPrimitive()) {
										JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
										if (arrayPrim.isNumber()) {// number
											tempFlag = tempFlag || arrayPrim.getAsFloat() <= queryPrim.getAsFloat();
										} else { // string, I don not think other type is comparable
											tempFlag = tempFlag || arrayPrim.getAsString().compareTo(queryPrim.getAsString()) <= 0;
										}
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						} else {flag = false;}
					}
					if (queryObj.has("$ne")) {
						isCompared = true;
						if (queryObj.get("$ne").isJsonPrimitive()) { // can only handle primitive type after Operators
							JsonPrimitive queryPrim = queryObj.getAsJsonPrimitive("$ne");
							if (result.isJsonPrimitive()) { // handle primitive result
								JsonPrimitive resultPrim = result.getAsJsonPrimitive();
								if (queryPrim.isNumber()) {// number
									flag = resultPrim.getAsFloat() != queryPrim.getAsFloat();
								} else { // string, I don not think other type is comparable
									flag = !resultPrim.getAsString().equals(queryPrim.getAsString());
								}
							} else if(result.isJsonArray()) { // handle Array result
								isArr = true;
								Boolean tempFlag = false; // if one match then match
								for (JsonElement arrayElem: (JsonArray) result) {
									if (arrayElem.isJsonPrimitive()) {
										JsonPrimitive arrayPrim = arrayElem.getAsJsonPrimitive();
										if (arrayPrim.isNumber()) {// number
											tempFlag = tempFlag || arrayPrim.getAsFloat() != queryPrim.getAsFloat();
										} else { // string, I don not think other type is comparable
											tempFlag = tempFlag || !arrayPrim.getAsString().equals(queryPrim.getAsString());
										}
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						} else {flag = false;}
					}
					
					if (queryObj.has("$nin")) {
						isCompared = true;
						if (!queryObj.get("$nin").isJsonArray()) flag = false;
						else {
							Boolean tempFlag = true;
							if (result.isJsonObject() || result.isJsonPrimitive()) {
								for (JsonElement queryJse : queryObj.getAsJsonArray("$nin")) {
									tempFlag = tempFlag && !queryJse.equals(result);
								}
								flag = tempFlag;
							}
							else if (result.isJsonArray()) {
								isArr = true;
								for (JsonElement resultElem : result.getAsJsonArray()) {
									for (JsonElement queryJse : queryObj.getAsJsonArray("$nin")) {
										tempFlag = tempFlag && !queryJse.equals(resultElem);
									}
								}
								arrFlag = arrFlag && tempFlag;
							}
						}
					}
					if (!isCompared) { // if not compare yet we compare the Object
						if (result.isJsonArray()) {
							Boolean tempFlag = false;
							for (JsonElement subElem:result.getAsJsonArray()) {
								tempFlag = tempFlag || queryObj.equals(subElem);
							}
							flag = tempFlag;
						} else flag = queryObj.equals(result);
					}
					if (isArr) {flag = arrFlag;}
				} else if (queryElem.isJsonArray()) {
					JsonArray queryArray = queryElem.getAsJsonArray();
					flag = queryArray.equals(result);
				}
			}
			if (flag) { // if it pass all condition
				if (cursorObjs.size() >= 1 && !isMultiple) return;
				JsonObject temp = docObj.deepCopy();
				if (isProject) {temp = this.projection(temp, fields);}
				cursorObjs.add(temp);
				indexs.add(iii);
			}
		}
		
		
		
		
		
		
		
		
	}
	public HashSet<Integer> getIndexs() {
		return this.indexs;
	}
	
	/**
	 * Returns true if there are more documents to be seen
	 */
	public boolean hasNext() {
		return index < cursorObjs.size();
	}

	/**
	 * Returns the next document
	 */
	public JsonObject next() {
		if (!hasNext()) return null;
		index+=1;
		return cursorObjs.get(index-1);
	}
	
	/**
	 * Returns the total number of documents
	 */
	public long count() {
		return cursorObjs.size();
	}

}
