package hw5;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

public class DB {

	/**
	 * Creates a database object with the given name.
	 * The name of the database will be used to locate
	 * where the collections for that database are stored.
	 * For example if my database is called "library",
	 * I would expect all collections for that database to
	 * be in a directory called "library".
	 * 
	 * If the given database does not exist, it should be
	 * created.
	 */
	private String curDB = null;
	private String collectionDir = null;
	public DB(String name) {
		File file = new File("testfiles/"+name);
		curDB = name;
		if (file.exists()) return;
		file.mkdir();
	}
	/**
	 * Retrieves the collection with the given name
	 * from this database. The collection should be in
	 * a single file in the directory for this database.
	 * 
	 * Note that it is not necessary to read any data from
	 * disk at this time. Those methods are in DBCollection.
	 */
	
	public DBCollection getCollection(String name) {
		collectionDir = "testfiles/"+curDB+"/"+name+".json";
		File file = new File(collectionDir);
		if (!file.exists()) {
			// if not exist create new json file
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new DBCollection(this,name);
	}
	public String getDir() {
		return collectionDir;
	}
	
	/**
	 * Drops this database and all collections that it contains
	 */
	public void dropDatabase() {
		if (curDB == null) return;
		File dir = new File("testfiles/"+curDB);
		if (dir.exists()) {
			if (dir.isDirectory()) {
				String[] children = dir.list();
				for(String cld : children) {
					File cldF = new File("testfiles/"+curDB+"/"+cld);
					cldF.delete();
				}
			}
			dir.delete();
		}
	}
}
